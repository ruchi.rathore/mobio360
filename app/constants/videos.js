// Final ID - 106
import * as images from '../assets/Images';
export let Videos360 = [

    {
        "id": 1,
        "contents": 24,
        "name": "Cities",
        "videos": [
            {
                "duration": 21334,
                "id": 1,
                "name": "The port city of Cartagena, Spain",
                "preview": images.portCities,
                "desc": "",
                "videoUrl": ""

            },
            {
                "duration": 21334,
                "id": 2,
                "name": "Kiev, Ukraine - City Plaza",
                "preview": images.kiev,
                "desc": "",
                "videoUrl": ""

            },
            {
                "duration": 21334,
                "id": 3,
                "name": "Elevated view across city",
                "preview": images.elevated,
                "desc": "",
                "videoUrl": ""

            },
            {
                "duration": 21334,
                "id": 4,
                "name": "Underwater view of large group of fish and river bed",
                "preview": images.underwater,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 5,
                "name": "Timelapse of city buildings illuminated at night",
                "preview": images.timelapse,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 6,
                "name": "View from diving board at Olympic swimming pool, London, UK, Europe",
                "preview": images.viewDiving,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 7,
                "name": "Timelapse of people looking at city, elevated view",
                "preview": images.timelapsePeople,
                "desc": "",
                "videoUrl": ""
            },

        ]
    },
    {
        "id": 2,
        "contents": 24,
        "name": "Underwater",
        "videos": [
            {
                "duration": 21334,
                "id": 8,
                "name": "Underwater Dive with a stunning Sea Turtl",
                "preview": images.underWaterDive,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 9,
                "name": "Underwater Bubble Fields #1,  Gulf of Mexico",
                "preview": images.underWaterBubble,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 10,
                "name": "Group of sea lions swimming underwater",
                "preview": images.groupSea,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 11,
                "name": "Two Manatees Underwater",
                "preview": images.twoManatee,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 12,
                "name": "Avanti Dive #2,  Dry Tortugas Florida",
                "preview": images.school,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 13,
                "name": "Underwater view of tropical fish by coral, Okinawa, Japan, Asia",
                "preview": images.tropical,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 14,
                "name": "Diver observing moray eel underwater, Cozumel, Mexico, Central America",
                "preview": images.diver,
                "desc": "",
                "videoUrl": ""
            },

        ]
    },
    {
        "id": 3,
        "contents": 24,
        "name": "Safari",
        "videos": [
            {
                "duration": 21334,
                "id": 15,
                "name": "Close up of Elephants on Safari, Africa",
                "preview": images.closeUpelephants,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 16,
                "name": "Tourists and safari keepers sitting by tree, Zambia, Africa",
                "preview": images.tourists,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 17,
                "name": "Tourists on hot air balloon above safari, Africa",
                "preview": images.touristsHotair,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 18,
                "name": "Tourist guide to safari resort, Bains Camp, Botswana, South Africa",
                "preview": images.touristsGuide,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 19,
                "name": "Observing family of lions sleeping from safari vehicle, Zambia, Africa",
                "preview": images.observing,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 20,
                "name": "Antelope Fleeing",
                "preview": images.antelope,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 21,
                "name": "Black Rhinos",
                "preview": images.rhinos,
                "desc": "",
                "videoUrl": ""
            },


        ]
    },
    {
        "id": 4,
        "contents": 24,
        "name": "Night Sky",
        "videos": [
            {
                "duration": 21334,
                "id": 22,
                "name": "Time lapse of night sky with Aurora Borealis over Bonnie Lake, Alaska, USA, North America",
                "preview": images.nightSky,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 23,
                "name": "Aurora Borealis lighting night sky over Butte, North Dakota, USA",
                "preview": images.aurora,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 24,
                "name": "Stars and milky way in night sky, Elbus, Russia",
                "preview": images.stars,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 25,
                "name": "View of statue and city at night, Quito, Ecuador, South America",
                "preview": images.statue,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 26,
                "name": "Aerial view of highway at night, Moscow, Russia",
                "preview": images.highway,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 27,
                "name": "Shop fronts and busy road at night",
                "preview": images.shop,
                "desc": "",
                "videoUrl": ""
            },
            {
                "duration": 21334,
                "id": 28,
                "name": "Sightseers walking along pedestrianised area and shops at night",
                "preview": images.sightseers,
                "desc": "",
                "videoUrl": ""
            },
        ]
    },
   
];