import * as action_types from './action_types';
import {console_log} from "../utils/helper";

export const smartTvData = (channels) => {
    return {
        type: action_types.SMART_TV,
        data: {
            smartTv: channels
        }
    }
};