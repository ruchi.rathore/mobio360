import * as action_types from './action_types';

export const functionName = (data) => {
    return {
        type: action_types.TEMPLATE_ACTIONS,
        data: data
    }
};