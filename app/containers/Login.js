import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import { Platform, StatusBar, TouchableOpacity, View, TouchableHighlight, TextInput, Text, Image, ImageBackground, AsyncStorage, KeyboardAvoidingView } from "react-native";
// Components
import Loader from '../components/Loader/Loader';
import NavigationService from '../utils/NavigationService';

// Styles
import { styles } from "../style/appStyles";
import loginStyles from "../style/loginStyles";

// Actions
import { checkAccess } from '../actions/WelcomeActions';
import { show, hide } from '../actions/ActivityIndicatorActions';

// Other data/helper functions
import { logo } from "../assets/Images";
import * as vars from '../constants/api';
import {PaymentAndroid} from '../components/Payment/PaymentAndroid';
//import {PaymentIOS} from '../components/Payment/PaymentIOS';
global.PaymentRequest = require('react-native-payments').PaymentRequest;
import Globals from  '../constants/Globals';
import SplashScreen from 'react-native-splash-screen';

//import PaymentRequest from 'react-native-payments';
class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accessCode: '',
            error: ''
        };
    }

    componentDidMount() {
       SplashScreen.hide();
    }

    getAccessToken = () => {
        if (this.state.accessCode !== '') {
            const url = vars.BASE_API_URL + `/checkAccess?uid=${this.state.accessCode.toLowerCase()}`;
            this.props.show();
            axios.get(url)
                .then(res => {
                    console.log('res:', res);
                    if (res.data.success === true) {
                        this.props.checkAccess(res.data.data.token);
                        AsyncStorage.setItem('@AccessToken:key', res.data.data.token);
                        NavigationService.reset("TabWatch");
                    } else {
                        this.setState({error: 'Please fill valid Access Code'});
                        this.props.hide();
                    }
                })
        } else {
            console.log('here:');
            this.setState({error: 'Please fill valid Access Code'});
        }
    }

    inAppPayment=()=>{
        if(Platform.OS==='ios') {
            //PaymentIOS();
        }
        else{
            PaymentAndroid(
                (data)=>{
                    if(data.purchaseState === 'PurchasedSuccessfully'){
                        this.setState({accessCode: 'fdf098fcc6'});
                        this.getAccessToken();
                    }
                },
                (error)=>{
                    //alert(error);
                    console.log(error);
                });
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={loginStyles.content} behavior={Platform.OS=='ios'?'padding':''}>

                <View style={{flex: 1}}>
                    <View style={loginStyles.logoView}>
                        <Image source={logo} style={loginStyles.logo} />
                    </View>
                    <View style={loginStyles.inputView}>
                        <Text style={[styles.avRegular, loginStyles.userNameText]}>{Globals.type == 'es' ? "CÓDIGO DE ACCESO" : 'ACCESS CODE'}</Text>
                        <View style={loginStyles.usernameView}>
                            {(this.state.error != '') ?
                                <Text style={[loginStyles.errorStyles]}>{this.state.error}</Text>
                                : null}
                            <TextInput
                                value={this.state.accessCode}
                                style={[styles.avRegular, loginStyles.inputV]}
                                onChangeText={(accessCode) => this.setState({ accessCode })}
                                keyboardType={"default"}
                                defaultValue={this.state.accessCode}
                                maxLength={40}
                                multiline={false}
                                autoCapitalize = "none"
                                underlineColorAndroid={'transparent'}
                            />
                        </View>
                    </View>
                    <View style={loginStyles.loginButtonView}>
                        <TouchableHighlight underlayColor="transparent" activeOpacity={0.6} onPress={() => this.getAccessToken()}>
                            <View style={loginStyles.loginButton}>
                                <Text style={[loginStyles.buttonText, styles.avRegular]}>
                                    {'CONTINUE'}
                                </Text>
                            </View>
                        </TouchableHighlight>

                        <TouchableOpacity style={{marginTop: 20, height: 40, alignSelf:'center'}} onPress = {()=> this.inAppPayment()}>
                            <Text style = {{color: '#fff'}}>{"New User?" }
                                <Text style = {{color: 'red'}}>{" Subscribe here" }</Text>
                            </Text>
                        </TouchableOpacity>

                    </View>

                </View>
                <Loader visible={this.props.loader.isLoading} />
            </KeyboardAvoidingView>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        access: state.WelcomeReducer,
        loader: state.ActivityIndicatorReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        checkAccess,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);

