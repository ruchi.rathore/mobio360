import React, { Component } from "react";
import { View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container } from "native-base";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
// Components
import Header from '../components/Header/Header';
// Styles
import { styles } from "../style/appStyles";
import channelListStyle from "../style/channelListStyle";
import favoriteStyles from '../style/favoriteStyle';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
// Other data/helper functions
import NavigationService from "../utils/NavigationService";
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import * as vars from '../constants/api';
import { console_log } from '../utils/helper';
import { messages } from '../constants/messages';
import MessageBar from '../components/Message/Message';
import { Videos360 } from '../constants/videos';
import Globals from  '../constants/Globals';

class Channels extends Component {
    constructor(props) {
        super(props);
        this.state = {
            favoriteSwitch: false,
            channels: [],
            videos: [],
            type: '',
            pageId: this.props.navigation.state.params.categoryId,
            categoryName: "",
            color: '',
            message:'',
            showMessage:false,
        };
    }

    componentDidMount(){
            let videoByCategory = Videos360.find((v) => v.id == this.state.pageId)['videos'],
                category = Videos360.find((v) => v.id == this.state.pageId);

            this.setState({
                videos: videoByCategory,
                type: 'videos',
                categoryName: category.name
            });
        
    }

    
    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    _handleFavoriteVideoClicked(data) {
        this.videoFavorite(data.video);
    }

    _onPressButton(data) {
        NavigationService.navigate('PlayOthers');
        this.props.getVideoOrChannelRelatedData(data);
    }

    onPressButtonOthers(data){
        NavigationService.navigate('PlayOthers');
        this.props.getVideoOrChannelRelatedData(data);
    }

    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    videoFavorite(data) {
        let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == data.id;
        });
        let videoToBeUpdated = {
            videoId: data.id,
            duration: data.duration,
            name: data.name,
            preview: data.preview
        };

        if (indexOf == -1) {
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.addToFavorites,
                        type: true
                    });
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    this.props.showMessage({
                        message: messages.removeFromFavorites,
                        type: false
                    });
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);
    }

    render() {
        let favoriteVideoIds = this.props.favorite.videos.map(v => v.videoId);
        let thisCategoryVideos = (!this.state.favoriteSwitch)? this.state.videos : this.state.videos.filter((video) => {
            if (~favoriteVideoIds.indexOf(video.id)) {
                return video;
            }
        });
        return (
            <Container>
                <ImageBackground  style={{ zIndex: 999 }}>
                    <Header
                        isDrawer={false}
                        isTitle={true}
                        title={this.state.categoryName}
                        isSearch={true}
                        showSearch={true}
                        rightLabel=''
                    />
                </ImageBackground>
                 <View style={channelListStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <ScrollView bounces={false} contentContainerStyle={{minHeight: Globals.IphoneX ?  Globals.deviceHeight - 140 : Globals.deviceHeight - 80}}>
                        <View style ={{flex: 3}}>
                        <View style={{ marginTop: 10,height: 40, flexDirection: 'row', padding: 5, justifyContent: 'space-between', alignItems: 'center' }}>
                            <View>
                                <Text style={[styles.avRegular, channelListStyle.categoryName]}>
                                    {this.state.categoryName}
                                </Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Text style={[styles.avRegular, channelListStyle.favoriteSwitchText]}>
                                    Favorites
                                </Text>
                                <Switch style={{ transform: [{ scaleX: .85 }, { scaleY: .75 }] }} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                            </View>
                        </View>
                        {<View style={{  flexDirection: 'row', flexWrap: 'wrap', paddingTop: 10 }}>
                            {thisCategoryVideos.length > 0 ?
                                thisCategoryVideos.map((v, index) => {
                                    return (
                                        <TouchableOpacity style={channelListStyle.imageThmbnail} key={index} onPress={this.onPressButtonOthers.bind(this, {video: v})}>
                                            <ImageBackground style={channelListStyle.imageBackground} resizeMode="stretch" source={v.preview}>
                                                <View style={{ flex: 1, backgroundColor: 'rgba(0,0,0,.5)' }}>
                                                    <TouchableOpacity style={channelListStyle.tvFavoriteBg} onPress={this._handleFavoriteVideoClicked.bind(this, {video: v})}>
                                                        <View style={channelListStyle.tvFavoriteView}>
                                                            <Icon name='star' size={Globals.DeviceType === 'Phone' ? 15 : 20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(v.id) ? "#FFC107" : "#fff"} />
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={channelListStyle.videoTitleView}>
                                                        <Text numberOfLines={1} style={[styles.avRegular, channelListStyle.videoTitle]}>{v.name}</Text>
                                                        <View style={channelListStyle.videoDurationView}>
                                                            <Text style={[styles.avRegular, channelListStyle.videoDuration]}>1h 40m</Text>
                                                        </View>
                                                    </View>
                                                </View>
                                            </ImageBackground>
                                        </TouchableOpacity>
                                    )
                                })
                                :
                                <View style = {{ flex: 2, alignItems: 'center', marginTop: 20, marginBottom: 20 }}>
                                        <View style={favoriteStyles.noChannelsViewTitle}>
                                            <Text style={[styles.avRegular, favoriteStyles.noData]}>{ "No Videos Added To Favorites Yet"}</Text>
                                        </View>
                                        <View style={favoriteStyles.noChannelsViewDesc}>
                                            <Text style={[styles.avRegular, favoriteStyles.noDataSubHeader]}>{ "Add your favorite videos to access and watch easily without any hassles."}</Text>
                                        </View>
                                        <TouchableHighlight  onPress={()=> NavigationService.goBack("null")} style={[favoriteStyles.exploreButtonView]}>
                                            <View style={[favoriteStyles.exploreButton, {backgroundColor: "#FC5969"}]}>
                                                <Text style={[favoriteStyles.buttonText, styles.avRegular]}>
                                                    { "Explore Videos"}
                                                </Text>
                                            </View>
                                        </TouchableHighlight>
                                </View>
                            }

                        </View>
                        }
                        </View>
                    </ScrollView>
                </View>
            </Container >
        );
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Channels);

