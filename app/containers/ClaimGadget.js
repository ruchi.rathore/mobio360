import React, { Component } from "react";
import { KeyboardAvoidingView, CameraRoll, View, TouchableHighlight, TextInput, Text, Image, ImageBackground, ScrollView, TouchableOpacity, Keyboard, AsyncStorage } from "react-native";
import { Container, CheckBox } from "native-base";
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import ModalSelector from 'react-native-modal-selector';
import Loader from '../components/Loader/Loader';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
// Components
import TabHeader from '../components/Header/TabHeader';
import Footer from '../components/Footer/Footer';
import Input from '../components/Input/Input';

// Styles
import { styles } from "../style/appStyles";
import accountStyles from "../style/accountStyles";
import Orientation from 'react-native-orientation';
// Other data/helper functions
import { background, defaultIcon, avatar } from "../assets/Images";
import { timezones } from "../utils/timezones";
import * as vars from '../constants/api';
import { messages } from '../constants/messages';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { showMessage } from '../actions/FlashMessageActions';
import { getDetails, setDetails, setProfilePic, getInterests } from '../actions/AccountActions';
import { setHeaderTitle } from '../actions/HeaderActions';
import { console_log, parseQueryString } from '../utils/helper';
import Globals from  '../constants/Globals';
import { checkAccess } from '../actions/WelcomeActions';

const accountTypes = [
    { label: 'Standard' },
    { label: 'Premium' }
];
import MessageBar from '../components/Message/Message';
var ImagePicker = require('react-native-image-picker');
class ClaimGadget extends Component {
    constructor(props) {
        super(props);
        this.state = {
            firstName: '',
            lastName:'',
            emailAddress: '',
            phoneNumber: '',
            selectedCode: '',
            selectedCountry:'',
            selectedState:'',
            zipCode: '',
            address: '',
            error: '',
            color: '',
            message:'',
            showMessage:false,
            selectedCountry: '',
           
        }
    }

    componentWillMount() {
    
    }

    componentWillReceiveProps(newProps){
        
    }

    componentDidMount() {
        Orientation.lockToPortrait();
        this.props.show();
        setTimeout(() => {
            this.props.hide();
        }, 1500);

    }

    editForm(e) {
        if (this.validateForm()) {
            Keyboard.dismiss();
            this.submitForm();
        }
    }


    validateForm() {
         let regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (this.state.firstName == '') {
            this.props.showMessage({
                message: messages.nameEmpty,
                type: false
            });
            this.setState({color:'red', message: messages.nameEmpty, showMessage: !this.state.showMessage})
            return false;
        }
        if (this.state.lastName == '') {
            this.props.showMessage({
                message: messages.nameEmpty,
                type: false
            });
            this.setState({color:'red', message: messages.nameEmpty, showMessage: !this.state.showMessage})
            return false;
        }
        if (this.state.emailAddress == '') {
            this.props.showMessage({
                message: messages.emailEmpty,
                type: false
            });
            this.setState({color:'red', message: messages.emailEmpty, showMessage: !this.state.showMessage})
            return false;
        }
        if (!regExp.test(this.state.emailAddress)) {
            this.props.showMessage({
                message: messages.emailNotValid,
                type: false
            });
            this.setState({color:'red', message: messages.emailNotValid, showMessage: !this.state.showMessage})
            return false;
        }
        // if (this.state.location == '') {
        //     this.props.showMessage({
        //         message: messages.locationEmpty,
        //         type: false
        //     });
        //     this.setState({color:'red', message: messages.locationEmpty, showMessage: !this.state.showMessage})
        //     return false;
        // }
        return true;
    }

    submitForm() {
        // let user = this.props.account.user;
        // user.name = this.state.name;
        // user.email = this.state.emailAddress;
        // user.address = this.state.location;
        // user.interests = this.state.interests;
        // user.accountType = this.state.selectedAccount;
        // user.timeZone = this.state.selectedTimezone;

        // axios.put(vars.BASE_API_URL+"/editProfile", user)
        //     .then((response) => {
        //         this.props.showMessage({
        //             message: messages.profileSaved,
        //             type: true
        //         });
        //         this.props.hide();
        //         //console_log(response);
        //         this.setState({color:'green', message: messages.profileSaved, showMessage: !this.state.showMessage})
        //     })
        //     .catch((error) => {
        //         this.props.hide();
        //         console_log(error);
        //     });
    }

    onChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    render() {
        let interest = this.props.account.interests;
        const  country = [{key: 1, label: 'Malaysia'}, {key: 2, label: 'India'}, {key: 3, label: 'UK'}];
        const  state = [{key: 1, label: 'state1'}, {key: 2, label: 'state2'}, {key: 3, label: 'state3'}];
        const code = [{key: 1, label: '+91'}, {key: 2, label: '+92'}, {key: 3, label: '+93'}];
        return (
            <Container>
                <KeyboardAvoidingView style={accountStyles.content} behavior={'padding'}>
                <ImageBackground style={{ zIndex: 10}}>
                <TabHeader
                    isDrawer={false}
                    isTitle={true}
                    title={'Claim Gadget'}
                    isSearch={false}
                    showSearch={false}
                />
                </ImageBackground>
                <Loader visible={this.props.loader.isLoading}/>
                <ScrollView style={accountStyles.content} >
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    <View source={background} style={{ flex: 1 , }}>
                            <View style={[accountStyles.viewWrapper]}>
                                <View style={{ width: '100%', marginTop: 20 }}>
                                        <View style={accountStyles.inputView}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{"First Name"}</Text>
                                            <View style={accountStyles.usernameView}>
                                                <TextInput
                                                    value={this.state.accessCode}
                                                    style={[styles.avRegular, accountStyles.input]}
                                                    errorText={this.state.error}
                                                    placeholder={'Enter First Name'}
                                                    ref={'firstName'}
                                                    onSubmitEditing={()=>this.refs.lastName.focus()}
                                                    placeholderTextColor={'#606060'}
                                                    onChangeText={(name) => this.setState({ firstName: name })}
                                                    defaultValue={this.state.firstName}
                                                    maxLength={40}
                                                    multiline={false}
                                                    returnKeyType={'next'}
                                                    underlineColorAndroid={'transparent'}
                                                />
                                            </View>
                                        </View>
                                        <View style={[accountStyles.inputView,{marginTop: 20}]}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{"Last Name"}</Text>
                                            <View style={accountStyles.usernameView}>
                                                <TextInput
                                                    value={this.state.accessCode}
                                                    style={[styles.avRegular, accountStyles.input]}
                                                    errorText={this.state.error}
                                                    placeholder={'Enter Last Name'}
                                                    ref={'lastName'}
                                                    onSubmitEditing={()=>this.refs.email.focus()}
                                                    placeholderTextColor={'#606060'}
                                                    onChangeText={(name) => this.setState({ lastName: name })}
                                                    defaultValue={this.state.lastName}
                                                    maxLength={40}
                                                    multiline={false}
                                                    returnKeyType={'next'}
                                                    underlineColorAndroid={'transparent'}
                                                />
                                            </View>
                                        </View>
                                        <View style={[accountStyles.inputView,{marginTop: 20}]}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{ "Email Address"}</Text>
                                            <View style={accountStyles.usernameView}>
                                                <TextInput
                                                    value={this.state.accessCode}
                                                    style={[styles.avRegular, accountStyles.input]}
                                                    errorText={this.state.error}
                                                    ref={'email'}
                                                    onSubmitEditing={()=>this.refs.phone.focus()}
                                                    placeholder={'Enter Email Address'}
                                                    placeholderTextColor={'#606060'}
                                                    onChangeText={(email) => this.setState({ emailAddress: email})}
                                                    defaultValue={this.state.emailAddress}
                                                    maxLength={40}
                                                    multiline={false}
                                                    returnKeyType={'next'}
                                                    underlineColorAndroid={'transparent'}
                                                />
                                            </View>
                                        </View>
                                        <View style={[accountStyles.inputView,{marginTop: 20}]}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{ "Phone Number"}</Text>
                                            <View style={{flexDirection: 'row',}}>
                                                <View style={accountStyles.pickerViewCode}>
                                                <ModalSelector
                                                    data={code}
                                                    initValue={'+91 '}
                                                    onChange={(selectedCode) => this.setState({ selectedCode: selectedCode.label })}
                                                    selectTextStyle={[styles.avRegular,accountStyles.input]}
                                                    selectStyle={{borderWidth: 0}}
                                                    cancelText={'Close'} />
                                                </View>
                                                <View style={[accountStyles.pickerViewText]}>
                                                <TextInput
                                                    value={this.state.accessCode}
                                                    style={[styles.avRegular, accountStyles.input]}
                                                    errorText={this.state.error}
                                                    ref={'phone'}
                                                    placeholderTextColor={'#606060'}
                                                    onChangeText={(phoneNumber) => this.setState({ phoneNumber: phoneNumber})}
                                                    defaultValue={this.state.phoneNumber}
                                                    maxLength={40}
                                                    multiline={false}
                                                    keyboardType={'phone-pad'}
                                                    underlineColorAndroid={'transparent'}
                                                />
                                            
                                        </View>
                                            </View>
                                        </View>
                                        <View style={[accountStyles.inputView,{marginTop: 20}]}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{ "Country"}</Text>
                                            <View style={accountStyles.pickerView}>
                                                <ModalSelector
                                                    data={country}
                                                    initValue={'Pick your Country '}
                                                    onChange={(selectedCountry) => this.setState({ selectedCountry: selectedCountry.label })}
                                                    selectTextStyle={[styles.avRegular,accountStyles.input]}
                                                    selectStyle={{borderWidth: 0}}
                                                    cancelText={'Close'} />
                                            </View>
                                        </View>
                                        <View style={[accountStyles.inputView,{marginTop: 20}]}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{ "State"}</Text>
                                            <View style={accountStyles.pickerView}>
                                                <ModalSelector
                                                    data={state}
                                                    initValue={'Pick your State '}
                                                    onChange={(selectedState) => this.setState({ selectedState: selectedState.label })}
                                                    selectTextStyle={[styles.avRegular,accountStyles.input]}
                                                    selectStyle={{borderWidth: 0}}
                                                    cancelText={'Close'} />
                                            </View>
                                        </View>
                                        <View style={[accountStyles.inputView,{marginTop: 20}]}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{ "Address"}</Text>
                                            <View style={accountStyles.usernameView}>
                                                <TextInput
                                                    value={this.state.accessCode}
                                                    style={[styles.avRegular, accountStyles.input]}
                                                    errorText={this.state.error}
                                                    ref={'Address'}
                                                    onSubmitEditing={()=>this.refs.zipcode.focus()}
                                                    placeholder={'Enter Address'}
                                                    placeholderTextColor={'#606060'}
                                                    onChangeText={(email) => this.setState({ address: email})}
                                                    defaultValue={this.state.address}
                                                    maxLength={40}
                                                    multiline={false}
                                                    returnKeyType={'next'}
                                                    underlineColorAndroid={'transparent'}
                                                />
                                            </View>
                                        </View>
                                        <View style={[accountStyles.inputView,{marginTop: 20}]}>
                                            <Text style={[styles.avRegular, accountStyles.userNameText]}>{ "Zip Code"}</Text>
                                            <View style={accountStyles.usernameView}>
                                                <TextInput
                                                    value={this.state.accessCode}
                                                    style={[styles.avRegular, accountStyles.input]}
                                                    errorText={this.state.error}
                                                    ref={'zipcode'}
                                                    placeholder={'Enter Zip Code'}
                                                    placeholderTextColor={'#606060'}
                                                    onChangeText={(email) => this.setState({ zipCode: email})}
                                                    defaultValue={this.state.zipCode}
                                                    maxLength={40}
                                                    multiline={false}
                                                    keyboardType={'numeric'}
                                                    returnKeyType={'done'}
                                                    underlineColorAndroid={'transparent'}
                                                />
                                            </View>
                                        </View>
                                            

                                          <View style ={{ alignSelf: 'center', height: 35, marginTop: 20, backgroundColor: '#FF5230', borderRadius: 5, width : 100, marginBottom: 80}}>
                                              <TouchableOpacity onPress={()=> this.editForm()}>
                                                <Text style={[styles.avRegular, accountStyles.btnSubmit]}>Submit</Text>
                                              </TouchableOpacity>
                                          </View>
                                </View>
                            </View>
                    </View>
                </ScrollView>
                </KeyboardAvoidingView>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        splash: state.SplashScreenReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        showMessage,
        getInterests,
        getDetails,
        setDetails,
        setProfilePic,
        checkAccess
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(ClaimGadget);