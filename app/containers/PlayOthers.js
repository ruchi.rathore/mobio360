import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import LinearGradient from 'react-native-linear-gradient';
import { Container, Content } from "native-base";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import axios from 'axios';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Orientation from 'react-native-orientation';
// Components
import Header from '../components/Header/PlayHeader';
import Footer from '../components/Footer/Footer';
// Styles
import { styles } from "../style/appStyles";
import PlayStyle from "../style/playStyle";

// Other data/helper functions
import { playBackground, movie_1, tv_4, tv_3 } from "../assets/Images";
import NavigationService from "../utils/NavigationService";
import * as vars from '../constants/api';
import { messages } from '../constants/messages';
import relatedVideosStyles from '../style/relatedVideosStyles';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
import { addFavoriteChannel, addLikesVideos, addFavoriteVideo } from '../actions/FavoriteActions';
import { showMessage } from '../actions/FlashMessageActions';
import { addChannelHistory } from '../actions/HistoryActions';
import { console_log } from '../utils/helper';
import { addVideoHistory } from '../actions/HistoryActions';
import Loader from '../components/Loader/Loader';
import { Videos360 } from '../constants/videos';
import MessageBar from '../components/Message/Message';
import Globals from  '../constants/Globals';
//import videoweb from '../medias/360-video.webm';
import video1 from '../medias/360-video.mp4';

class PlayOthers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            favoriteSwitch: false,
            isPlaying: false,
            playUrl: '',
            dataLoad: false,
            color: '',
            message:'',
            showMessage:false,
        };
    }


    _switchFavorite() {
        this.setState({ favoriteSwitch: !this.state.favoriteSwitch });
    }

    _onPressButton(data) {
        this.setState({isPlaying: false, playUrl: ''});
        this.Scrollview.scrollTo({x: 0,y:0, animated: true});
         this.props.getVideoOrChannelRelatedData(data);
    }

    componentWillMount() {
        if (this.props.play.data.video) {
            this.getVideoCategories(this.props.play.data.video.id);
        }
    }

    componentDidMount() {
        //this.props.show();
        setTimeout(() => {
            this.props.hide();
        }, 1500);
        this.setState({dataLoad: true});
        this.getVideo(this.props.play.data.video.id);
    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.play.data.video.id != nextProps.play.data.video.id) {
            this.getVideo(nextProps.play.data.video.id);
            this.getVideoCategories(nextProps.play.data.video.id);
        }
    }

    getVideoCategories(videoId) {
            let videoCategory = Videos360.find((category) => {
                let videoIds = category.videos.map(v => v.id);
                if (~videoIds.indexOf(videoId)) {
                    return category;
                }
            });
            if (videoCategory) {
                let video = videoCategory.videos.find((v) => v.id == videoId);
                this.setState({
                    data: Object.assign({video: video}, {category: videoCategory})
                });
            } else {

            }

    }

    isVideoFavorite(videoId) {
        let indexOf = this.props.favorite.videos.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    addRemoveFavorite(video) {
        this.videoFavorite(video);
    }

    videoFavorite(video) {
        let favoriteVideos = this.props.favorite.videos;
        let indexOf = favoriteVideos.findIndex((f) => {
            return f.videoId == video.id;
        });
        let videoToBeUpdated = {
            videoId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        if (indexOf == -1) {
            this.props.showMessage({
                message: messages.addToFavorites,
                type: true
            });
            favoriteVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/favorites/videos", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'green', message: messages.addToFavorites, showMessage: !this.state.showMessage})
        } else {
            this.props.showMessage({
                message: messages.removeFromFavorites,
                type: false
            });
            favoriteVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/favorites/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
            this.setState({color:'red', message: messages.removeFromFavorites, showMessage: !this.state.showMessage})
        }

        this.props.addFavoriteVideo(favoriteVideos);
    }

    playVideo() {
        
        this.props.navigation.navigate('Player', {url: video1});
        this.addHistoryVideo(this.state.data.video);
    }

    addHistoryVideo(video) {
        let historyVideos = this.props.historyVideos.videos;
        let indexOf = historyVideos.findIndex((f) => {
            return f.videoId == video.id;
        });
        let videoToBeUpdated = {
            videoId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        if (indexOf == -1) {
            historyVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/history/videos", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        }

        this.props.addVideoHistory(historyVideos);
    }

    getVideo(videoId) {
           // this.props.show();
            axios.get(vars.BASE_API_URL+"/getVideoStreamingURL/"+videoId+"/WIFI")
                .then((response) => {
                    this.setState({
                        playUrl: response.data.data.stream.url
                    });
                    this.props.hide();
                })
                .catch((error) => {
                    this.props.hide();
                    console_log(error);
                });
    }



    addLike(video) {
        this.videoLike(video);
    }

    videoLike(video) {
        let likedVideos = this.props.favorite.videosLiked;
        let indexOf = likedVideos.findIndex((f) => {
            return f.videoId == video.id;
        });

        let videoToBeUpdated = {
            videoId: video.id,
            duration: video.duration,
            name: video.name,
            preview: video.preview
        };

        if (indexOf == -1) {
            likedVideos.push(videoToBeUpdated);
            axios.post(vars.BASE_API_URL+"/likes/videos", videoToBeUpdated)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        } else {
            likedVideos.splice(indexOf, 1);
            axios.delete(vars.BASE_API_URL+"/likes/videos/"+videoToBeUpdated.videoId)
                .then((response) => {
                    //console_log(response);
                })
                .catch((error) => {
                    console_log(error);
                });
        }

        this.props.addLikesVideos(likedVideos);
    }

    isVideoLiked(videoId) {
        let indexOf = this.props.favorite.videosLiked.findIndex((f) => {
            return f.videoId == videoId;
        });

        if (indexOf != -1) {
            return true;
        }
        return false;
    }

    _handleFavoriteClicked(data) {
        this.videoFavorite(data.video);
    }

    _renderPlayContent(video) {
        if(video.video !==undefined) {
            return (
                    <View>
                        <View style={PlayStyle.imageContainer}>
                            <Image style={{height: 155, width: 250}} source={video.video.preview}/>
                        </View>
                        <View style={{paddingTop: 40}}>
                            <View style={PlayStyle.playButtonView}>
                                {this.state.playUrl !== null ?
                                    <TouchableOpacity onPress={this.playVideo.bind(this)} underlayColor="transparent" activeOpacity={0.6}>
                                    <View style={PlayStyle.playButton}>
                                        <Icon name="play" size={18}
                                              style={{backgroundColor: 'transparent', paddingRight: 10}} color="#fff"/>
                                        <Text style={[PlayStyle.buttonText, styles.avRegular]}>
                                            PLAY
                                        </Text>
                                    </View>
                                </TouchableOpacity>: null}

                            </View>
                        </View>
                    </View>

            )
        }
    }


    renderLoadVOD=(video)=>{
        return(
            <View>
                {video.category?
                    <View style={[relatedVideosStyles.wrapperView]}>
                        <View style={[relatedVideosStyles.textAndSwitchView]}>
                            <View>
                                <Text style={[styles.avRegular, PlayStyle.headerText]}>
                                    {"Related Videos"}
                                </Text>
                            </View>
                            <View style={[relatedVideosStyles.switchWrapper]}>
                                <Text style={[styles.avRegular, PlayStyle.subHeaderText]}>
                                    Autoplay
                                </Text>
                                <Switch style={[relatedVideosStyles.switch]} value={this.state.favoriteSwitch} onValueChange={this._switchFavorite.bind(this)} />
                            </View>
                        </View>
                        <View>
                            <View>
                                {video.category.videos.map((item, index) => {
                                    if (item.id != video.video.id) {
                                        return (
                                            <View style={[relatedVideosStyles.relatedItemView]} key={index} onPress={this._onPressButton.bind(this, {video: item})}>
                                                <View style={[relatedVideosStyles.imageWrapperVideo]}>
                                                    <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item})}>
                                                        <ImageBackground style={relatedVideosStyles.imageBackgroundVideo} source={item.preview}>
                                                            <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}></View>
                                                        </ImageBackground>
                                                    </TouchableOpacity>
                                                </View>
                                                <TouchableOpacity onPress={this._onPressButton.bind(this, {video: item})} style={{flexDirection: 'column',flex: 5,paddingLeft: '3%',paddingVertical: '10%'}}>
                                                    <Text style={[styles.avRegular, PlayStyle.relatedItemTitleOther]}>{item.name}</Text>
                                                    <View style={[relatedVideosStyles.titleIcon]}>
                                                        <FeatherIcon name='play-circle' size={Globals.DeviceType === 'Phone' ?  20 : 30} style={[relatedVideosStyles.relatedItemIcon]} color='#8165a8' />
                                                        <Text style={[styles.avRegular, PlayStyle.relatedItemSubTitle]}>{item.duration}</Text>
                                                    </View>
                                                </TouchableOpacity>
                                                <TouchableOpacity style={[relatedVideosStyles.relatedItemIconWrapper]} onPress={this.addRemoveFavorite.bind(this, item)}>
                                                    <Icon name='star' size={Globals.DeviceType === 'Phone'? 16 : 21} style={[relatedVideosStyles.relatedItemIcon]} color={this.isVideoFavorite(item.id) ? "#FFC107" : "#fff"} />
                                                </TouchableOpacity>
                                            </View>
                                        )
                                    }
                                })
                                }
                            </View>
                        </View>
                    </View>
                    : null}
            </View>
        )
    }

    render() {
        let video = this.state.data;
        return (
            <Container>
                <ImageBackground source={playBackground} style={{ zIndex: 999 }}>
                    <Header
                        isDrawer={false}
                        isTitle={true}
                        title={video.video.name}
                        isSearch={true}
                        rightLabel=''
                    />
                </ImageBackground>
                <Loader visible={this.props.loader.isLoading} />
                {/*<Search from={"videos"}/>*/}
                <View style={PlayStyle.contentView}>
                    <MessageBar showMessage={this.state.showMessage} color={this.state.color} message={this.state.message}/>
                    {video.video ?
                    <ScrollView ref={component => this.Scrollview = component} bounces={false}>
                        <LinearGradient colors={['#8165a8',  'transparent', '#000', '#000', ]}
                                        style={{ flex: 3, zIndex: 10, marginTop: 0, }}
                        >
                            <View>
                                {this._renderPlayContent(video)}
                            </View>

                        <View style={{ paddingRight: '4%', paddingTop: '4%' }}>

                            <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: '4%'}} >
                                <View style={{ flex: 5 }}>
                                    <Text numberOfLines={5} style={[styles.avRegular, PlayStyle.videoTitle]}>{video.video.name}</Text>
                                </View>
                                <View style={{ alignItems: 'center', flex: 2 }}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.addRemoveFavorite.bind(this, video.video)}>
                                        <Icon name='star' size={20} style={{ backgroundColor: 'transparent' }} color={this.isVideoFavorite(video.video.id) ? "#FFC107" : "#fff"} />
                                    </TouchableOpacity>
                                    <Text style={[styles.avRegular, PlayStyle.watchLaterText]}>Favorites</Text>
                                </View>
                                <View style={PlayStyle.rateView}>
                                    <TouchableOpacity style={{ alignItems: 'center' }} onPress={this.addLike.bind(this, video.video)}>
                                        <Icon name='thumbs-up' size={22} style={{ backgroundColor: 'transparent' }} color = {this.isVideoLiked(video.video.id) ? "#699bff" : "#fff"} />
                                    </TouchableOpacity>
                                    <Text style={[styles.avRegular, PlayStyle.rateText]}>Rate</Text>
                                </View>
                            </View>
                            <View style={[PlayStyle.descriptionView, {width: '100%'}]}>
                                <View style={PlayStyle.durationView}>
                                    <Text style={[styles.avRegular, PlayStyle.durationText]}>PG-13 | </Text>
                                    <Text style={[styles.avRegular, PlayStyle.durationText]}>2h 6min | </Text>
                                    <Text style={[styles.avRegular, PlayStyle.durationText]}>15 August 2014 (USA)‎</Text>
                                </View>
                                <View>
                                    <Text style={[styles.avRegular, PlayStyle.durationText, { color: '#2770ba' }]}>{(video.category) ? video.category.name : ""}</Text>
                                </View>
                            </View>
                            <View style = {{marginTop: 12, marginBottom : 12, paddingLeft: '4%', paddingRight: '4%' }}>
                                <Text style = {[styles.avRegular,{color : '#ffffff', fontSize: 12, }]}>{(video.video.desc)? video.video.desc : ""}</Text>
                            </View>
                        </View>
                        {this.state.dataLoad? this.renderLoadVOD(video) : null}
                        <View style={{ paddingBottom: 20 }}>
                        </View>
                        </LinearGradient>
                    </ScrollView >
                        :
                        null}
                </View>
            </Container >
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
        splash: state.SplashScreenReducer,
        user: state.AuthenticationReducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
        addFavoriteChannel,
        addFavoriteVideo,
        showMessage,
        addChannelHistory,
        addVideoHistory,
        addLikesVideos
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayOthers);

