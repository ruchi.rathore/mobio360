import React, { Component } from "react";
import { View, TouchableHighlight, Text, Image, ImageBackground, ScrollView, TouchableOpacity } from "react-native";
import { Container } from "native-base";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import axios from 'axios';
import * as vars from '../constants/api';
import { show, hide } from '../actions/ActivityIndicatorActions';
import { setHeaderTitle } from '../actions/HeaderActions';
import { clearHistory, addVideoHistory, addChannelHistory, addShowHistory, } from '../actions/HistoryActions';
import { getVideoOrChannelRelatedData } from '../actions/PlayActions';
// Components
import TabHeader from '../components/Header/TabHeader';
import Footer from '../components/Footer/Footer';
import HistoryItems from '../components/HistoryItem/HistoryItem';

// Styles
import HistoryStyles from "../style/historyStyle";
import { styles } from "../style/appStyles";

// Other data/helper functions
import { console_log, fromNow } from '../utils/helper';
import Globals from  '../constants/Globals';

class History extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }

    componentDidMount() {
        this.props.show();
        setTimeout(() => {
            this.props.hide();
        }, 1500);
    }

    clearHistory() {
        axios.delete(vars.BASE_API_URL+"/history")
            .then((response) => {
            })
            .catch((error) => {
                console_log(error);
            });
        this.props.clearHistory();
    }

    componentWillMount() {
        axios.get(vars.BASE_API_URL+"/history")
            .then((history) => {
                if (history.data.data) {
                    this.props.addVideoHistory(history.data.data.videos);
                }
            })
            .catch((error) => {
                this.props.hide();
                console_log(error);
            });

    }

    render() {
        return (
            <Container>
                <TabHeader
                    isDrawer={false}
                    isTitle={true}
                    title={'History'}
                    isSearch={true}
                    showSearch={false}
                    rightLabel={'Clear' }
                    rightClick={this.clearHistory.bind(this)}
                />
                        <HistoryItems data={this.props.historyVideos.channels}/>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        header: state.HeaderReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        setHeaderTitle,
        clearHistory,
        addChannelHistory,
        addVideoHistory,
        addShowHistory,
        getVideoOrChannelRelatedData,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(History);
