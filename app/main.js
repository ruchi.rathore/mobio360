import React, { Component } from "react";
import { StatusBar, View, Text, TextInput, AsyncStorage, NetInfo, Alert, Dimensions, ImageBackground, Platform, Image } from "react-native";
import { StackNavigator } from "react-navigation";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { checkAccess } from './actions/WelcomeActions';
// Components
import ChannelList from "./containers/ChannelList";
import Login from "./containers/Login";
import PlayOthers from "./containers/PlayOthers";
import Player from "./components/Player/Player";
import { splashBg, background } from "./assets/Images";

import TabWatch from './components/Navigator/TabWatch';
import Orientation from 'react-native-orientation';
// Other data/functions
import NavigationService from "./utils/NavigationService";
// let originalGetDefaultProps = Text.getDefaultProps;
// Text.getDefaultProps = function() {
//   return {
//     ...originalGetDefaultProps(),
//     allowFontScaling: false,
//   };
// };
TextInput.defaultProps.allowFontScaling=false;
import Globals from './constants/Globals';

const RootNavigator = StackNavigator({
    TabWatch: {
        screen: TabWatch,
        navigationOptions: {
            title: 'TabWatch',
            header: null,
            gesturesEnabled: true
        }
    },
    ChannelList: {
        screen: ChannelList,
        navigationOptions: {
            title: 'Channels',
            header: null,
            gesturesEnabled: true
        }
    },
    PlayOthers: {
        screen: PlayOthers,
        navigationOptions: {
            title: 'PlayOthers',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    Player: {
        screen: Player,
        navigationOptions: {
            title: 'Player',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    Login: {
        screen: Login,
        navigationOptions: {
            title: 'Access Id',
            header: null,
            gesturesEnabled: true
        }
    },

});

const LoginNavigator = StackNavigator({
    Login: {
        screen: Login,
        navigationOptions: {
            title: 'Access Id',
            header: null,
            gesturesEnabled: true
        }
    },
    ChannelList: {
        screen: ChannelList,
        navigationOptions: {
            title: 'Channels',
            header: null,
            gesturesEnabled: true
        }
    },
    PlayOthers: {
        screen: PlayOthers,
        navigationOptions: {
            title: 'PlayOthers',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    Player: {
        screen: Player,
        navigationOptions: {
            title: 'Player',
            header: null,
            gesturesEnabled: true,
            headerBackTitle: null
        }
    },
    TabWatch: {
        screen: TabWatch,
        navigationOptions: {
            title: 'TabWatch',
            header: null,
            gesturesEnabled: true
        }
    },

});


 class Main extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            accessToken: ''
        }
      }

      componentDidMount() {
        Orientation.lockToPortrait();
        AsyncStorage
            .getItem("@AccessToken:key")
            .then((value) => {
            if (value !== null) {
                this.setState({ accessToken: value, });
                this.props.checkAccess(value);
            } else {
                this.setState({ accessToken: null, });
            }
        })
        .done();

         setTimeout(() => {
            this.setState({isLoading: false});
         },1000);
    }

      render() {
            if (this.state.isLoading) {
                return (
                    <View style ={{width : Globals.deviceWidth,height: Globals.deviceHeight, backgroundColor: '#000'}}>
                    
                    </View>
                )
            } else {
                    if (this.state.accessToken !== null) {
                        return <RootNavigator ref={navigatorRef => {
                            NavigationService.setTopLevelNavigator(navigatorRef);
                        }} />
                    } else {
                        return <LoginNavigator ref={navigatorRef => {
                            NavigationService.setTopLevelNavigator(navigatorRef);
                        }} />
                    }      
                }
        }
    }

    const mapStateToProps = (state) => {
        return {
            access: state.WelcomeReducer,  
        };
    };
    
    const mapDispatchToProps = (dispatch) => {
        return bindActionCreators({
            checkAccess,
        }, dispatch);
    };
    
    export default connect(mapStateToProps, mapDispatchToProps)(Main);