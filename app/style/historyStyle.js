import {StyleSheet, Platform, Dimensions} from "react-native";
import * as FontSizes from '../utils/fontsSizes';
const deviceHeight = Dimensions
    .get("window")
    .height;
const deviceWidth = Dimensions
    .get("window")
    .width;
import Globals from '../constants/Globals';
import * as color from '../utils/color';

export default StyleSheet.create({
    content: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: color.background
    },
    historyItem: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        //backgroundColor: 'green', height: 110
    },
    historyTitle: {
        flex: 3,
        alignItems: 'flex-start',
        paddingLeft: '2%',
        justifyContent: 'center'

    },
    imageBackgroundVOD: {
        width: Globals.DeviceType === 'Phone'
            ? 75
            : 122,
        height: Globals.DeviceType === 'Phone'
            ? 113
            : 179,
        //marginLeft: 10
    },
    imageBackgroundVOD_BD: {
        width: Globals.DeviceType === 'Phone'
            ? 105
            : 162,
        height: Globals.DeviceType === 'Phone'
            ? 123
            : 219,
        //marginLeft: 10
    },
    historyContent: {
        //minHeight: deviceHeight - 190,
        backgroundColor: color.background,
        paddingLeft: '3%',
        paddingRight: '3%'
    },
    historyItemTitle: {
        color: color.lightPurple,
        fontSize: FontSizes.medium
    },
    historyItemDuration: {
        color: color.lightPurple,
        fontSize: FontSizes.xSmall,
        paddingTop: 4
    },
    imageBackground: {
        width: '100%',
        height: Globals.DeviceType == 'Phone'
            ? 85
            : 135,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imageBackground_BD: {
        width: '100%',
        height: Globals.DeviceType === "Phone"
            ? 85
            : 135,
        alignItems: 'center',
        justifyContent: 'center'
    },
    channelLogo: {
        width: "55%",
        height: '55%',
        resizeMode: 'contain',
        marginTop: '17%',
        alignSelf: 'center'
    },
    bgOpacity: {
        backgroundColor: 'rgba(0,0,0,.5)',
        width: '100%',
        height: '100%'
    },
    mainContainer: {
        flex: 1,
        backgroundColor: color.background,
        //top: Platform.OS == "ios" ? ((deviceHeight == 812) ? 95 : 65) : 45,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 10,
        width: deviceWidth,
        alignItems: "center",
        //position: "absolute",
        justifyContent: "center",
    },
    searchHelp: {
        marginTop: 10,
        backgroundColor: color.background,
        flex: 1
    }
});
