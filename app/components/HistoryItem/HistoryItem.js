import React, { Component } from 'react';
import {
    TouchableHighlight,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    ImageBackground,
    Image
} from 'react-native';
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
// Styles
import historyStyles from "../../style/historyStyle";
import { styles } from "../../style/appStyles";
import {HistoryStyles} from '../../style/historyStyle';

// Other data/helper functions
import { banner, channelLogo } from "../../assets/Images";
import { show, hide } from '../../actions/ActivityIndicatorActions';
import { getVideoOrChannelRelatedData } from '../../actions/PlayActions';
import * as vars from '../../constants/api';
import {thumbnail} from "../../assets/Images";
import { console_log, fromNow } from '../../utils/helper';
import NavigationService from "../../utils/NavigationService";
import Globals from  '../../constants/Globals';
import { Videos360 } from '../../constants/videos';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import MaterialIcons from 'react-native-vector-icons/dist/MaterialIcons';

class HistoryItem extends Component {
    constructor(props) {
        super(props)
        this.state = {
         }
    }

    componentWillReceiveProps(newProps){


    }

    onPressButton(data) {
        this.props.getVideoOrChannelRelatedData(data);
        if (data.channel) {
            NavigationService.navigate('Play');
        } else {
            NavigationService.navigate('PlayOthers');
        }
    }

    onPressButtonOthers(data) {
         this.props.getVideoOrChannelRelatedData(data);
            NavigationService.navigate('PlayVOD');
    }

    onPressButtonOthers_vd(data) {
         this.props.getVideoOrChannelRelatedData(data);
        NavigationService.navigate('PlayOthers');
    }

    getCreatedDate=(vid)=>{
        let id;
        this.props.historyVideos.shows.map((shows,i)=>{
            if(shows.showId === vid){
                id =  shows.createdAt;
            }
        })
        return id;
    }

    getCreatedDateVideo=(vid)=>{
        let id;
        this.props.historyVideos.videos.map((videos,i)=>{
            if(videos.videoId === vid) {
                id = videos.createdAt;
            }
        })
        return id;
    }

    render() {
         const { data } = this.props;
         let historyVideoIds =  this.props.historyVideos.videos.map(v => v.videoId);

        let history360 = [].concat.apply([], Videos360.map((c) => c.videos)).filter((v) => {
            if (~historyVideoIds.indexOf(v.id)) {
                return v;
            }
        });
        console.log('history360:', history360);
         return (
            <View style={{flex: 1, backgroundColor: '#000'}}>
                {((this.props.historyVideos.videos && this.props.historyVideos.videos.length > 0)) ?
                    <ScrollView >
                        {history360.map((video, index) => {
                            console.log('**History:', video.preview);
                                return (
                                    <View key={index} style={{borderBottomColor :  '#606060',
                                        borderBottomWidth: 1,
                                        paddingTop: 5
                                    }}>
                                        <View style={[historyStyles.historyItem, {height: 110}]} key={index} >
                                            <View style={{ flex: 2 }}>
                                                <TouchableOpacity style = {{ marginTop: 10, marginBottom: 10 }} onPress={
                                                    this.onPressButtonOthers_vd.bind(this,
                                                        {video: {id: video.id,
                                                    name: video.name,
                                                    preview: video.preview,
                                                    duration: video.duration
                                                }})}>
                                                    <ImageBackground source={video.preview} style={[historyStyles.imageBackgroundVOD_BD,{marginLeft: 10}]} resizeMode={"contain"} >

                                                    </ImageBackground>
                                                </TouchableOpacity>
                                            </View>
                                            <TouchableOpacity style={historyStyles.historyTitle} onPress={
                                                this.onPressButtonOthers_vd.bind(this,
                                                    {video: {id: video.id,
                                                    name: video.name,
                                                    preview: video.preview,
                                                    duration: video.duration
                                                }})}>
                                                <Text style={[styles.avRegular, historyStyles.historyItemTitle]}>{video.name}</Text>
                                                <Text style={[styles.avRegular, historyStyles.historyItemDuration]}>{fromNow(this.getCreatedDateVideo(video.id))}</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )
                            })
                        }
                   </ScrollView>

                        :
                        <View style = {{ flex: 1, alignItems: 'center' }}>
                            <MaterialIcons name={'history'} size={Globals.DeviceType=== 'Phone' ? 100: 150} style={{ backgroundColor: 'transparent', alignSelf: 'center', marginTop: '48%' }} color= {'#e08058'} />
                            <Text style={[styles.avRegular,{color: '#A4A4A4', fontSize: 18}]}>{Globals.type === 'es'? "Sin historial de videos todavía" : 'No History Videos / Channels Yet'}</Text>
                        </View>
                }
            </View>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        accessToken: state.WelcomeReducer.token,
        account: state.AccountReducer,
        category: state.CategoryReducer,
        country: state.CountryReducer,
        domain: state.DomainReducer,
        favorite: state.FavoriteReducer,
        flashmessage: state.FlashMessageReducer,
        header: state.HeaderReducer,
        historyVideos: state.HistoryReducer,
        loader: state.ActivityIndicatorReducer,
        play: state.PlayReducer,
        smartTV: state.SmartTVReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        show,
        hide,
        getVideoOrChannelRelatedData,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(HistoryItem);




