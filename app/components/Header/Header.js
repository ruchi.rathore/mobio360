import React, { Component } from "react";
import { Keyboard, Animated, StatusBar, Dimensions, Image, View, Text, TouchableHighlight, TouchableOpacity, Platform, TextInput } from "react-native";
import { Header, Button, Body, Left, Right, Item, Input } from "native-base";
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NavigationService from '../../utils/NavigationService';

// Styles
import { styles } from '../../style/appStyles';
import headerStyle from '../../style/headerStyle';
import { showSearchBar, HideSearchBar, onShowSearchView } from '../../actions/HeaderActions';
import { searchText } from '../../actions/SearchActions';
// Other data/helper functions
import { headerLogo, loginLogo } from "../../assets/Images";
import {console_log} from "../../utils/helper";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Globals from '../../constants/Globals';


class HeaderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearch: false,
            x: new Animated.Value(0),

        };
    }

    searchOpen() {
        NavigationService.navigate('Search');

    }

    componentWillReceiveProps(newProps){
        if(newProps.header.hideSearchBar){
            this.slideOut();
        }
    }

    clearText() {
        this.props.searchText('');
    }

    onChange(text) {
        this.props.searchText(text);
    }

    _renderLeftSection(checkLeft) {
        const { isDrawer } = this.props;
        if (isDrawer) {
            return(null)
        } else {
            return (
                
                <TouchableHighlight underlayColor="transparent" activeOpacity={0.2} style={headerStyle.iconsView} onPress={() => NavigationService.goBack()}>
                    <Icon name="angle-left" size={26} style={{ backgroundColor: 'transparent', marginLeft: 10 }} color="#fff" />
                </TouchableHighlight>
                  
            )
        }
    }


    slide = () => {
        this.setState({x: deviceWidth});
        this.TextInput.focus();
    };

    slideOut = () => {
        this.setState({x: 0});
        this.TextInput.blur();
    };

    render() {
        const { isDrawer, isTitle, title, isSearch, rightLabel, rightClick, showSearch } = this.props;
        return (
            isSearch?
                <View style={headerStyle.header}>
            <View style={[headerStyle.header, {flexDirection: 'row', justifyContent: 'center'}]}>
                {Platform.OS == 'ios'?
                    <StatusBar backgroundColor="#fff" barStyle="light-content"/>
                    : <StatusBar backgroundColor="#000000" barStyle="light-content"/>}
                <View style={headerStyle.headerBg}>
                            <TouchableOpacity style={headerStyle.leftIconView}  onPress={() => {alert('here'); NavigationService.goBack()}}>
                                {this._renderLeftSection(title)}
                            </TouchableOpacity>
                            <View style={ [headerStyle.titleView, {justifyContent: 'center', height: '100%'}]}>
                                {
                                    isTitle ?
                                        <Text style={[styles.avRegular, headerStyle.title]}>{title}</Text>
                                        : <Image style={[headerStyle.logo]} source={loginLogo} />
                                }
                            </View>

                <View style={ [headerStyle.rightIconView]}>
                    {
                        showSearch ?
                            null
                            : <TouchableOpacity  style={[headerStyle.iconsView]} onPress={() => {rightClick()}}>
                                {title === 'History' ?
                                    <FeatherIcon name={'trash-2'} size={Globals.DeviceType === 'Phone' ?  22 : 30} style={{ backgroundColor: 'transparent', alignSelf: 'flex-end' }} color= {'#a8a8a8'} />
                                    :
                                    <Text style={[styles.avRegular, headerStyle.rightText, {alignSelf: 'flex-end'}]}>{rightLabel}</Text>

                                    }

                            </TouchableOpacity>
                    }
                </View>
                </View>
            </View>
                </View>
                    :
                <View style={headerStyle.header}>
                    {!this.state.showSearch ?
                        <View style={headerStyle.SearchDefaultBar}>
                            <TouchableOpacity onPress={()=>{this.setState({showSearch: true})}}
                                style={{width: '100%', alignItems: 'center', flexDirection: 'row'}}>
                                <FeatherIcon color="#585858" size={20} name="search"
                                             style={headerStyle.defltsearchIcn}/>
                                <Text style={[styles.avRegular, headerStyle.dfltSearch]}>Search</Text>
                            </TouchableOpacity>
                        </View>
                        :
                        <View searchBar style={[headerStyle.header, {flexDirection: 'row'}]}>
                            {Platform.OS == 'ios'?
                                <StatusBar backgroundColor="#fff" barStyle="light-content"/>
                                : <StatusBar backgroundColor="#000000" barStyle="light-content"/>}
                            <View style={headerStyle.headerSearch}>
                                <FeatherIcon color="#8165a8" size={20} name="search" style={headerStyle.searchIcn} />
                                <TextInput
                                    value= {this.props.search.searchText}
                                    underlineColorAndroid={'transparent'}
                                    style={[styles.avRegular,headerStyle.searchTxt]}
                                    placeholder={Globals.type === 'es'? "Buscar.." : 'Search..'}
                                    placeholderTextColor={'#808080'}
                                    ref={(ref) => this.TextInput = ref}
                                    autoFocus={true}
                                    onChangeText={(text)=> this.onChange(text)} />
                                <TouchableOpacity onPress={()=> { this.props.searchText(''); }}>
                                    <FeatherIcon color="#8165a8" size={17} name="x-circle" style={{marginTop : Platform.OS == "ios" ? ((deviceHeight == 812) ? 10 : 10) : 10 }} />
                                </TouchableOpacity>
                            </View>
                            <Button style = {headerStyle.btn} onPress={()=>{
                                this.setState({showSearch: false});
                                this.props.showSearchBar(false);
                                this.props.searchText('');
                                this.props.onShowSearchView(false);
                                NavigationService.goBack();
                                Keyboard.dismiss();
                            }} transparent>
                                <Text style={[styles.avRegular, headerStyle.cancelTxt]}>{Globals.type === 'es'? "Cancelar" : "Cancel"}</Text>
                            </Button>
                        </View>

                    }

                </View>


        );
    }
}


const mapStateToProps = (state) => {
    return {
        header: state.HeaderReducer,
        search: state.SearchReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        searchText,
        showSearchBar,
        HideSearchBar,
        onShowSearchView
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(HeaderComponent);
