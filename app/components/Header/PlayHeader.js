import React, { Component } from "react";
import { Keyboard, Animated, StatusBar, Dimensions, Image, View, Text, TouchableHighlight, TouchableOpacity, Platform, TextInput, ImageBackground } from "react-native";
import { Header, Button, Body, Left, Right, Item, Input } from "native-base";
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import NavigationService from '../../utils/NavigationService';
import { playBackground } from "../../assets/Images";
// Styles
import { styles } from '../../style/appStyles';
import headerStyle from '../../style/headerStyle';
import { showSearchBar, HideSearchBar, onShowSearchView} from '../../actions/HeaderActions';
import { searchText } from '../../actions/SearchActions';
// Other data/helper functions
import { headerLogo } from "../../assets/Images";
import {console_log} from "../../utils/helper";
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Globals from '../../constants/Globals';

class PlayHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSearchBar: false,
            x: new Animated.Value(0),
        };
    }

    searchOpen() {
        this.clearText();
        NavigationService.navigate('Search');
    }
    componentWillReceiveProps(newProps){
       if(newProps.header.hideSearchBar){
            this.slideOut();
        }
    }

    clearText() {
        this.props.searchText('');
    }

    onChange(text) {
        this.props.searchText(text);
    }

    slide = () => {
        this.setState({x: deviceWidth});
        this.TextInput.focus();
    };

    slideOut = () => {
        this.setState({x: 0});
        this.TextInput.blur();
    };

    _renderLeftSection() {
        const { isDrawer } = this.props;
        if (isDrawer) {
            return (
                <TouchableHighlight underlayColor="transparent" activeOpacity={0.2} style={headerStyle.iconsView} onPress={() => NavigationService.navigate("DrawerOpen")}>
                    <FeatherIcon name="menu" size={26} style={{ backgroundColor: 'transparent', marginLeft: 10 }} color="#fff" />
                </TouchableHighlight>
            )
        } else {
            return (
                <TouchableHighlight underlayColor="transparent" activeOpacity={0.2} style={headerStyle.iconsView} onPress={() => NavigationService.goBack()}>
                    <Icon name="angle-left" size={26} style={{ backgroundColor: 'transparent', marginLeft: 10 }} color="#fff" />
                </TouchableHighlight>
            )
        }
    }
    render() {
        const { isDrawer, isTitle, title, isSearch, rightLabel, rightClick, showSearch } = this.props;
        return (
            <View style={[headerStyle.header, {backgroundColor: '#315687'}]}>
                <View style={[headerStyle.header, {flexDirection: 'row', justifyContent: 'center', backgroundColor: '#8165a8'}]}>
                    {Platform.OS == 'ios'?
                        <StatusBar backgroundColor="#fff" barStyle="light-content"/>
                        : <StatusBar backgroundColor="#8165a8" barStyle="light-content"/>}
                    <View style={headerStyle.headerBg}>
                        <View style={headerStyle.leftIconView}>
                            {this._renderLeftSection()}
                        </View>
                        <View style={ [headerStyle.titleView, {justifyContent: 'center', height: '100%'}]}>
                            {
                                isTitle ?
                                    <Text style={[styles.avRegular, headerStyle.title]}>{title}</Text>
                                    : <Image style={[headerStyle.logo]} source={headerLogo} />
                            }
                        </View>
                        <View style={ headerStyle.rightIconView}>
                            {
                                showSearch ?
                                    null
                                    : <TouchableOpacity  onPress={() => {rightClick()}}>
                                        <Text style={[headerStyle.rightText]}>{rightLabel}</Text>
                                    </TouchableOpacity>
                            }
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}


const mapStateToProps = (state) => {
    return {
        header: state.HeaderReducer,
        search: state.SearchReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        searchText,
        showSearchBar,
        HideSearchBar,
        onShowSearchView
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(PlayHeader);
