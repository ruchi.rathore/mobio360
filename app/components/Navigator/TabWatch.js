
import React from 'react';

import {Image, Platform} from 'react-native';
import { TabNavigator,TabBarBottom } from "react-navigation";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import MaterialIcon from 'react-native-vector-icons/dist/MaterialIcons';
import {createIconSetFromIcoMoon} from 'react-native-vector-icons';
import icoMoonConfig from '../../assets/icomoon.json';
const Icomoon = createIconSetFromIcoMoon(icoMoonConfig);
// Const Values
//import * as colors from '../utils/colors';
//import * as FontSizes from '../utils/fontsSizes';

// Components
import Account from '../../containers/Accounts';
import VOD from '../../containers/VOD';
import Search from '../../components/Search/Search';
import ClaimGadget from '../../containers/ClaimGadget';
import Globals from '../../constants/Globals';
import * as color from '../../utils/color';
import { vr } from '../../assets/Images';

const TabWatch = TabNavigator({
    VOD: {
        screen: VOD,
        key: 'VOD',
        navigationOptions: {
            tabBarLabel :  'Home',
            tabBarIcon: ({ focused, tintColor }) => <MaterialIcon name={'home'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#FC5969" : '#8165a8'} />
        },

    },
    Search: {
        screen: Search,
        navigationOptions: {
            tabBarLabel:  'Search',
            tabBarIcon: ({ focused, tintColor }) => <FeatherIcon name={'search'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#FC5969" : '#8165a8'} />
        },
    },
    // History: {
    //     screen: History,
    //     navigationOptions: {
    //         tabBarLabel: 'History',
    //         tabBarIcon: ({ focused, tintColor }) => <MaterialIcon name={'history'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#FC5969" : '#8165a8'} />
    //     },
    // },
    ClaimGadget: {
        screen: ClaimGadget,
        navigationOptions: {
            tabBarLabel: 'Claim Gadget',
            tabBarIcon: ({ focused, tintColor }) => <Icomoon name={"vr"} size={Globals.DeviceType=== 'Phone' ? 22: 30}  style={{ backgroundColor: 'transparent', height: Globals.DeviceType=== 'Phone' ? 22: 30, width:  Globals.DeviceType=== 'Phone' ? 22: 30}} color= {focused ? "#FC5969" : '#8165a8'} />
        },
    },
    Account: {
        screen: Account,
        navigationOptions: {
            tabBarLabel: 'Account',
            tabBarIcon: ({ focused, tintColor }) => <Icon name={'user-circle'} size={Globals.DeviceType=== 'Phone' ? 22: 30} style={{ backgroundColor: 'transparent' }} color= {focused ? "#FC5969" : '#8165a8'} />
        },
    },
}, {
    tabBarPosition: 'bottom',
    animationEnabled: false,
    //lazyLoad: false,
    lazy: false,
    overflow: 'hidden',
    //swipeEnabled: false,
    tabBarOptions: {
        initialRouteName: 'VOD',
        showIcon: true,
        inactiveTintColor : '#8165a8',
        activeTintColor: '#FC5969',
        labelStyle: {
            fontSize: Globals.DeviceType=== 'Phone' ? 11: 19,
            fontFamily: 'AvenirNextLTW01RegularRegular',
            marginTop: Globals.DeviceType=== 'Phone' ? Platform.OS == "ios" ? -5  : 1 : 1
        },
        style: {
            backgroundColor: color.background,

            // height: Globals.DeviceType === 'Phone' ? 43: 70
        },
        indicatorStyle: {
            backgroundColor: '#000',
        },
    },
});

export default TabWatch;
