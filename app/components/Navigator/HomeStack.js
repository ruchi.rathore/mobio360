import React from 'react';

import { StackNavigator } from 'react-navigation';

// Components

import VOD from '../../containers/VOD';
import LiveChannels from '../../containers/LiveChannels';
import Home from '../../containers/Home';
// Styles

// Other data/helper functions

const HomeStack = StackNavigator({
        Home: {
            screen: Home,
            navigationOptions: {
                title: 'Home',
                header: null,
                gesturesEnabled: true
            }
        },
        VOD: {
            screen: VOD,
            navigationOptions: {
                title: 'VOD',
                header: null,
                gesturesEnabled: true
            }
        },
        LiveChannels: {
            screen: LiveChannels,
            navigationOptions: {
                title: 'Live Channels',
                header: null,
                gesturesEnabled: true
            }
        },
},
{initialRouteName: 'Home'});


export default HomeStack;
