import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import HistoryStyles from "../../style/historyStyle";
import headerStyle from "../../style/headerStyle";
import { Keyboard, Dimensions, Platform, Modal, Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import { Container, Content, Item, Button, Input } from "native-base";
import { Videos360 } from '../../constants/videos';
import * as vars  from '../../constants/api';
import axios from 'axios';
import { showSearchBar, HideSearchBar, onShowSearchView } from '../../actions/HeaderActions';
import { searchText } from '../../actions/SearchActions';
import { getVideoOrChannelRelatedData } from '../../actions/PlayActions';
import { console_log } from '../../utils/helper';
import {thumbnail} from "../../assets/Images";
import { styles } from "../../style/appStyles";
import liveChannelStyle from "../../style/liveChannelStyle";
import FeatherIcon from 'react-native-vector-icons/dist/Feather';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import NavigationService from '../../utils/NavigationService';
import Footer from '../Footer/Footer';
import Globals from  '../../constants/Globals';
const deviceHeight = Dimensions.get("window").height;
const deviceWidth = Dimensions.get("window").width;
import Header from '../../components/Header/Header';

let bidiotvMovies =  Videos360;//Globals.type === 'es' ?  esmobiotv : bidiotvMoviesData;

class SearchPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: this.props.from,
            showSearchBar: false,
            searchText: '',
        };

      
    }

    componentWillMount() {

    }

    componentWillUnmount() {
        this.props.searchText('');
    }

    _onPressButtonVODChannel(data) {
        this.props.getVideoOrChannelRelatedData(data);
        NavigationService.navigate('PlayOthers');
    }

    onChange(text) {
        this.props.searchText(text);
    }

    openViewCategory(categoryId, type){
            NavigationService.navigate('ChannelList',{from: 'VOD',categoryId: categoryId});    
    }

    


    render() {
        var regEx = new RegExp(this.props.search.searchText, 'i');
        let videoSearch, chkvalue;
        if(this.props.search.searchText.trim(' ') !=='') {
            
        }
        // let  getchannelSearch, videoSearch , packageVideos, chkvalue = 0, arrChannelcat = [];
        // if(this.props.search.searchText.trim(' ') !=='') {

        //      getchannelSearch = this.props.category.channels.filter((channel) => {
        //         if (regEx.test(channel.channelName)) {
        //             return channel
        //         }
        //     });

        //     this.props.category.categories.sort((a, b) => a.id > b.id).map((category, i) => {
        //         getchannelSearch.map((channel, index) => {
        //             let categoryIds = channel.channelCategories.map((c) => c.categoryId);
        //             if (~categoryIds.indexOf(category.categoryId)) {
        //                  arrChannelcat.push(category.categoryId);
        //              }
        //         });

        //     });
        // }
         return (
            //this.props.header.searchView === true?
            <Container>
                <Header
                    isDrawer={false}
                    isTitle={true}
                    title={'Search'}
                    isSearch={false}
                    rightLabel=''
                />
            <View
                style={HistoryStyles.mainContainer}>
                <View style={{ flex: 1 }}>
                    {this.props.search.searchText.trim(' ') !=='' ?
                        <ScrollView style={[HistoryStyles.content, {width: deviceWidth}]} keyboardShouldPersistTaps={'always'} keyboardDismissMode='on-drag' contentContainerStyle={{minHeight: Globals.IphoneX ?  Globals.deviceHeight - 140 : Globals.deviceHeight - 80 }}>
                            <View style={{flex: 1}}>
                                <View style={[HistoryStyles.historyContent, { marginTop: 10, paddingLeft: 0, paddingRight: 0, flex: 3}]}>
                                        {Videos360.sort((a, b) => a.id > b.id).map((videos, i) => {
                                            videoSearch = videos.videos.filter((video) => {
                                                if (regEx.test(video.name)) {
                                                    return video;
                                                }
                                            });     
                                            if(videoSearch.length > 0){
                                                chkvalue = videoSearch.length;
                                                return (
                                                    <View key = {i}>
                                                        <View style={{ height: 25, flexDirection: 'row', marginLeft: 5, marginRight: 5, justifyContent: 'space-between', backgroundColor: '#3A3A3A', alignItems: 'center' }}>
                                                            <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.categoryName, {color: '#8165a8', marginLeft: 5}]}>
                                                                {videos.name}
                                                            </Text>
                                                        </View>
                                                        {
                                                            <ScrollView keyboardShouldPersistTaps={'always'} showsHorizontalScrollIndicator={false} horizontal={true} style={{marginTop: 10}}>
                                                                <View style={{ flexDirection: 'row' }}>
                                                                    {videoSearch.map((v, k) => {
                                                                        return (
                                                                            <View style={liveChannelStyle.tvThmbnail} key={k}>
                                                                                <TouchableOpacity onPress={this._onPressButtonVODChannel.bind(this, {video: v})}>
                                                                                    <ImageBackground style={liveChannelStyle.tvImageBackground} source={v.preview}>
                                                                                        <View style={{flex: 1, backgroundColor: "rgba(0,0,0,.5)"}}>
                                                                                            <View style={liveChannelStyle.videoTitleView}>
                                                                                                <Text numberOfLines={1} style={[styles.avRegular, liveChannelStyle.videoTitle]}>{v.name}</Text>
                                                                                                <View style={[liveChannelStyle.videoDurationView]}>
                                                                                                    <Text style={[styles.avRegular, liveChannelStyle.videoDuration,{paddingBottom: 1}]}>1h 40m</Text>
                                                                                                </View>
                                                                                            </View>
                                                                                        </View>
                                                                                    </ImageBackground>
                                                                                </TouchableOpacity>
                                                                            </View>
                                                                        )
                    
                                                                    },this)
                                                                    }
                                                                </View>
                                                            </ScrollView>
                                                        }
                    
                                                    </View>
                                                )
                                            }
                    
                                        })
                                    }
                                    {
                                        (videoSearch.length === 0 && chkvalue === 0)?
                                        <View style = {{alignItems : 'center', flex: 1}}>
                                            <Text style={[styles.avRegular, {color: '#fff', marginTop: 20, alignSelf: 'center'}]}>{"No Results Found"}</Text>
                                        </View>
                                        : null
                                    }
                                </View>
                             
                            </View>
                        {/*<Footer />*/}
                        </ScrollView>
                :
                        <ScrollView bounces={false} keyboardShouldPersistTaps={'always'} keyboardDismissMode='on-drag' style ={HistoryStyles.searchHelp}>
                            {
                                Videos360.sort((a, b) => a.id > b.id).map((videos, i) => {
                                    return (
                                        <TouchableOpacity key = {i} style={{alignItems: 'center', marginTop: 20}} onPress={()=> {this.openViewCategory(videos.id, 'Vodchannels'); Keyboard.dismiss();}}>
                                            <Text numberOfLines={1} style={[styles.avRegular, {color: '#8165a8', fontSize: Globals.DeviceType === 'Phone' ?  16 : 20}]}>
                                                {videos.name}
                                            </Text>
                                        </TouchableOpacity>
                                    )
                                })


                            }
                        </ScrollView>

                    }

                </View>
            </View>
            </Container>
                // :null
        )
    }
}

const mapStateToProps = (state) => {
    return {
       category: state.CategoryReducer,
        country: state.CountryReducer,
        favorite: state.FavoriteReducer,
        header: state.HeaderReducer,
        historyVideos: state.HistoryReducer,
        play: state.PlayReducer,
        search: state.SearchReducer,
    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        searchText,
        getVideoOrChannelRelatedData,
        showSearchBar,
        HideSearchBar,
        onShowSearchView
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchPage);
