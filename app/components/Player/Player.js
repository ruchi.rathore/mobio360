import React, { Component } from 'react';
import {
    TouchableHighlight,
    View,
    Text,
    ScrollView,
    TouchableOpacity,
    Platform, StyleSheet
} from 'react-native';
import Orientation from 'react-native-orientation';
import Video from 'react-native-af-video-player';
import VideoPlayer from 'react-native-video-controls';
import NavigationService from "../../utils/NavigationService";
import {
    ViroScene, Viro360Image, ViroVideo, ViroText, ViroVRSceneNavigator
  } from 'react-viro';

var sharedProps = {
    apiKey:"AD265D70-F0F5-4AA0-9647-56F2456141F7",
}
var InitialVRScene = require('./HelloWorldScene');
  
class Player extends Component {

    constructor(props) {
        super(props);
        this.state = {
            playUrl: this.props.navigation.state.params.url,
            sharedProps : sharedProps
        };
    }

    static navigationOptions = ({ navigation }) => {
        const { state } = navigation
        // Setup the header and tabBarVisible status
        const header = state.params && (state.params.fullscreen ? undefined : null)
        const tabBarVisible = state.params ? state.params.fullscreen : true
        return {

            // For stack navigators, you can hide the header bar like so
            header,
            // For the tab navigators, you can hide the tab bar like so
            tabBarVisible,
        }
    }

    onFullScreen(status) {
        // Set the params to pass in fullscreen status to navigationOptions
        this.props.navigation.setParams({
            fullscreen: false,//!status
        })
    }

    componentWillUnmount() {
        Orientation.lockToPortrait();
    }

    componentWillMount() {
        this.props.navigation.setParams({
            fullscreen: false,
        });
        Orientation.getOrientation((err, orientation) => {
            console.log(`Current Device Orientation: ${orientation}`);
        });
        Orientation.lockToLandscape();
        Orientation.unlockAllOrientations();
    }

    onMorePress() {

    }

    render() {
        return (
            <ViroVRSceneNavigator 
            {...this.state.sharedProps}
             initialScene={{scene: InitialVRScene}}
             onExitViro={()=> {
                 NavigationService.goBack("null");
                Orientation.lockToLandscape();
                }}
             />
        )
    }
}

export default Player;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#000'
    }
})
