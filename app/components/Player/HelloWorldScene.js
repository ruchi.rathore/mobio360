'use strict';

import React, {Component} from 'react';

import {StyleSheet} from 'react-native';

import {
    ViroScene,
    ViroText,
    Viro360Image,
    Viro360Video
} from 'react-viro';

export default class HelloWorldScene extends Component {

    constructor() {
        super();

        this.state = {} // Set initial state here
    }

    render() {
        return (
            <ViroScene>
                <Viro360Video source={require('../../medias/360-video.mp4')}/>
            </ViroScene>
        );
    }

}

var styles = StyleSheet.create({
    helloWorldTextStyle: {
        fontFamily: 'Arial',
        fontSize: 60,
        color: '#ffffff',
        textAlignVertical: 'center',
        textAlign: 'center',
    },
});

module.exports = HelloWorldScene;
