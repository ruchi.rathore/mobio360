import React, { Component } from "react";
import { Image, View, TouchableHighlight, Text, ImageBackground, ScrollView, Switch, TouchableOpacity } from "react-native";
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Swiper from 'react-native-swiper';
import LinearGradient from 'react-native-linear-gradient';
import Globals from '../../constants/Globals';
// Components

// Styles
import { styles } from "../../style/appStyles";
import sliderStyles from "../../style/sliderStyles";

// Other data / functions
import { banner }  from "../../assets/Images";
import NavigationService from "../../utils/NavigationService";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getVideoOrChannelRelatedData } from '../../actions/PlayActions';

class Slider extends Component {
    constructor(props) {
        super(props);
        this.renderDot = this.renderDot.bind(this);
        this.renderActiveDot = this.renderActiveDot.bind(this);
    }

    renderDot() {
        return (
            <View style={[sliderStyles.dots]} />
        )
    }

    renderActiveDot() {
        return (
            <View style={[sliderStyles.dotsActive]} />
        )
    }

    render() {
        return (
                <Swiper
                    loop={true}
                    autoplay={true}
                    autoplayTimeout={4}
                    dot={this.renderDot()}
                    paginationStyle={[sliderStyles.paginationStyle]}
                    activeDot={this.renderActiveDot()}>
                    <View style={sliderStyles.indicatorViewPage}>
                        <Image style={[sliderStyles.slides, {position: 'absolute'}]} resizeMode="stretch"  source={require('../../assets/images/banner.jpg')} ></Image>
                        <Image style={{height: 80, width: 135, marginTop: 120, alignSelf: 'center'}} source={require('../../assets/images/people.png')}/>
                    </View>
                </Swiper>

        );
    }
}

const mapStateToProps = (state) => {
    return {

    };
};

const mapDispatchToProps = (dispatch) => {
    return bindActionCreators({
        getVideoOrChannelRelatedData,
    }, dispatch);
};

export default connect(mapStateToProps, mapDispatchToProps)(Slider);