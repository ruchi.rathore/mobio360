import * as action_types from '../actions/action_types';
import { console_log } from "../utils/helper";

const initialState = {
    smartTv: []
};

export const SmartTVReducer = (state = initialState, action = {}) => {
    switch (action.type) {
        case action_types.SMART_TV:
            return {
                ...state,
                smartTv: action.data.smartTv
            };
        default:
            return state;
    }
};