import * as action_types from '../actions/action_types';
import { console_log } from "../utils/helper";

const initialState = {
    videos: [],
    channels: [],
    shows:[]
};

export const HistoryReducer = (state = initialState, action = {}) => {
     switch (action.type) {
        case action_types.ADD_HISTORY_CHANNEL:
            return {
                ...state,
                channels: action.data
            };
        case action_types.ADD_HISTORY_VIDEO:
            return {
                ...state,
                videos: action.data
            };
        case action_types.ADD_HISTORY_SHOW:
            return{
                ...state,
                shows: action.data
            };
        case action_types.CLEAR_HISTORY:
            return {
                videos: [],
                channels: [],
                shows: []
            };
        default:
            return state;
    }
};