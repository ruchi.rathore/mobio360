import { combineReducers } from 'redux';
import drawer from "./DrawerReducer";
import splashScreen from "./SplashScreenReducer";
import { AccountReducer } from './AccountReducer';
import { ActivityIndicatorReducer } from './ActivityIndicatorReducer';
import { AuthenticationReducer } from './AuthenticationReducer';
import { CountryReducer } from './CountryReducer';
import { CategoryReducer } from './CategoryReducer';
import { DomainReducer } from './DomainReducer';
import { FlashMessageReducer } from './FlashMessageReducer';
import { FavoriteReducer } from './FavoriteReducer';
import { HeaderReducer } from './HeaderReducer';
import { HistoryReducer } from './HistoryReducer';
import { PlayReducer } from './PlayReducer';
import { SearchReducer } from './SearchReducer';
import { SmartTVReducer } from './SmartTVReducer';
import { WelcomeReducer } from './WelcomeReducer';

export default combineReducers({
    AccountReducer,
    ActivityIndicatorReducer,
    AuthenticationReducer,
    CategoryReducer,
    CountryReducer,
    DomainReducer,
    FlashMessageReducer,
    FavoriteReducer,
    HeaderReducer,
    HistoryReducer,
    PlayReducer,
    SearchReducer,
    SmartTVReducer,
    WelcomeReducer,
    drawer,
    splashScreen
});